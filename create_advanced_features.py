#!/usr/bin/env python3

__author__ = "bo wang"
__email__ = "wang818@usc.edu"

from collections import namedtuple
import csv
import glob
import os
import sys

pre_speaker = False
firstcome = "undefined"
dictionary = {'qw':'f','sd':'f','ba':'b' ,'+':'o','x':'o','%':'o','b':'b','fo_o_fw_"_by_bc':'o','qy':'f','ny':'b','bh':'b','bk':'b','sv':'f','aa':'b','^q':'o','aap_am':'b','h':'o','qh':'f','qo':'f','ng':'b','qy^d':'f','^h':'b','bf':'b','br':'b','nn':'b','^g':'f','b^m':'b','^2':'b','ad':'f','qrr':'f','ar':'b','fc':'f','no':'b','na':'b','ft':'f','t3':'o','qw^d':'f','oo_co_cc':'f','fa':'f','bd':'b','t1':'o','fp':'f','arp_nd':'b'}
def get_utterances_from_file(dialog_csv_file):
    """Returns a list of DialogUtterances from an open file."""
    reader = csv.DictReader(dialog_csv_file)
    for du_dict in reader:
        _dict_to_dialog_utterance(du_dict)
    print("")


def get_utterances_from_filename(dialog_csv_filename):
    """Returns a list of DialogUtterances from an unopened filename."""
    global firstcome, pre_speaker
    firstcome = False
    pre_speaker = "undefined"
    with open(dialog_csv_filename, "r") as dialog_csv_file:
        get_utterances_from_file(dialog_csv_file)

def _dict_to_dialog_utterance(du_dict):
    """Private method for converting a dict to a DialogUtterance."""
    global firstcome, pre_speaker,dictionary
     # Remove anything with 
    for k, v in du_dict.items():
        if len(v.strip()) == 0:
            du_dict[k] = None
    result = ""
    if du_dict["act_tag"]:
        result += du_dict["act_tag"]  # + "\t" + dictionary[du_dict["act_tag"]]
    else:
        result += "UNK"
    if pre_speaker == "undefined":
        if du_dict["speaker"]:
            pre_speaker = du_dict["speaker"]
    else:
        if du_dict["speaker"]:
            if not du_dict["speaker"] == pre_speaker:
                result += "\tYES"
                pre_speaker = du_dict["speaker"]        
    if firstcome == False:
        firstcome = True
        result += "\tYES"
    # Extract tokens and POS tags
    token_list = []
    pos_list = []
    if du_dict["pos"]:
        for token_pos_pair in du_dict["pos"].split():
            token_list.append(token_pos_pair.split("/")[0])
            pos_list.append(token_pos_pair.split("/")[1])
    for item in token_list:
        result += '\t'+"token_"+item
    for item in pos_list:
        result += '\t'+"pos_"+item
    j = 0
    while j < len(token_list):
        if j>=1:
            result += '\t'+"bigram_"+token_list[j-1]+"_"+token_list[j]
        j += 1
    i = 0
    while i < len(pos_list):
        if i>=1:
            result += '\t'+"bipos_"+pos_list[i-1]+"_"+pos_list[i]
        i += 1
    result +=  '\t'+ str(len(token_list))
    print(result)

if __name__ == '__main__':
    if not len(sys.argv) == 2:
        print("arguments number error")
        exit(0)

    get_utterances_from_filename(sys.argv[1]);
