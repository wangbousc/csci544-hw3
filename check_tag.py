import sys
import os
import csv
import glob

dictionary = {}

def load_dir(dirname):
	for filename in os.listdir(dirname):
		get_utterances_from_filename(dirname+"/"+filename)

def get_utterances_from_file(dialog_csv_file):
    """Returns a list of DialogUtterances from an open file."""
    reader = csv.DictReader(dialog_csv_file)
    for du_dict in reader:
        _dict_to_dialog_utterance(du_dict)


def get_utterances_from_filename(dialog_csv_filename):
    """Returns a list of DialogUtterances from an unopened filename."""
    with open(dialog_csv_filename, "r") as dialog_csv_file:
    	get_utterances_from_file(dialog_csv_file)

def _dict_to_dialog_utterance(du_dict):
    """Private method for converting a dict to a DialogUtterance."""
    global dictionary
    if not du_dict["act_tag"] in dictionary:
    	dictionary[du_dict["act_tag"]] = True
    	print(du_dict["act_tag"])

load_dir(sys.argv[1])