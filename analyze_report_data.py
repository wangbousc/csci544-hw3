import os
import sys
if not len(sys.argv) == 2:
    print("arguments number error")
    exit(0)
file = open(sys.argv[1],"r")
sum = 0.0
count = 0
for line in file.readlines():
    curline = line.rstrip()
    if "Item accuracy:" in curline:
        sum += float(curline[-6:-1])
        count += 1
print(sum/count)