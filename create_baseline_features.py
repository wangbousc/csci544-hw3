#!/usr/bin/env python3

__author__ = "bo wang"
__email__ = "wang818@usc.edu"

from collections import namedtuple
import csv
import glob
import os
import sys

pre_speaker = False
firstcome = "undefined"

def get_utterances_from_file(dialog_csv_file):
    """Returns a list of DialogUtterances from an open file."""
    reader = csv.DictReader(dialog_csv_file)
    for du_dict in reader:
        _dict_to_dialog_utterance(du_dict)
    print("")


def get_utterances_from_filename(dialog_csv_filename):
    """Returns a list of DialogUtterances from an unopened filename."""
    global firstcome, pre_speaker
    firstcome = False
    pre_speaker = "undefined"
    with open(dialog_csv_filename, "r") as dialog_csv_file:
        get_utterances_from_file(dialog_csv_file)

def _dict_to_dialog_utterance(du_dict):
    """Private method for converting a dict to a DialogUtterance."""
    global firstcome, pre_speaker
     # Remove anything with 
    for k, v in du_dict.items():
        if len(v.strip()) == 0:
            du_dict[k] = None
    result = ""
    if du_dict["act_tag"]:
        result += du_dict["act_tag"]
    else:
        result += "UNK"
    if pre_speaker == "undefined":
        if du_dict["speaker"]:
            pre_speaker = du_dict["speaker"]
    else:
        if du_dict["speaker"]:
            if not du_dict["speaker"] == pre_speaker:
                result += "\tYES"
                pre_speaker = du_dict["speaker"]        
    if firstcome == False:
        firstcome = True
        result += "\tYES"
    # Extract tokens and POS tags
    token_list = []
    pos_list = []
    if du_dict["pos"]:
        for token_pos_pair in du_dict["pos"].split():
            token_list.append(token_pos_pair.split("/")[0])
            pos_list.append(token_pos_pair.split("/")[1])
    for item in token_list:
        result += '\t'+"token_"+item
    for item in pos_list:
        result += '\t'+"pos_"+item
    print(result)

if __name__ == '__main__':
    if not len(sys.argv) == 2:
        print("arguments number error")
        exit(0)

    get_utterances_from_filename(sys.argv[1]);
