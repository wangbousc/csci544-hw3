import create_baseline_features
import sys
import os

def load_dir(dirname):
	for filename in os.listdir(dirname):
		create_baseline_features.get_utterances_from_filename(dirname+"/"+filename)

if not len(sys.argv) == 2:
    print("arguments number error")
    exit(0);
load_dir(sys.argv[1])